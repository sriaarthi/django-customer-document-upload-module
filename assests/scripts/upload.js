function renderTableOnload(data) {
    var tableBody = document.getElementsByTagName("tbody");
    var htmlStr = '';
    var formNames = data && data.names && data.names.length > 0 ? data.names : [];
    // loop through all elements in the array, building a table row for element
    for ( i = 0; i < formNames.length; i++) {
        htmlStr += '<tr index="' + i + '">';
        htmlStr += '<td>' + formNames[i] + '</td>';
        htmlStr += '<td class="file-field input-field"><div class="btn btn-small"><span>Browse</span><input type="file" name="document" accept="application/pdf,.doc,.docx"></div><div class="file-path-wrapper"><input class="file-path validate path-' + i + '" type="text"></div></td>';
        htmlStr += '<td><i class="material-icons delete" onclick="clearInput(event)">clear</i></td>';
        htmlStr += '</tr>';
    };
    // add the htmlstr to tbody
    $(tableBody).html(htmlStr);
};

function toggleLoader() {
    $('#page-loader').show(); 
    $('#upload-form').hide();
    return true;
}

function clearInput(event) {
    var index = parseInt($(event.target).parents().eq(1)[0].getAttribute("index"));
    $(".file-path.path-"+index).val('');
    event.stopPropagation();
}

$( document ).ready(function() {
    var data = {
        "names": [
        "Loan Application Form",
        "Form 4506-T : Request For Transcript Of Tax Return",
        "Personal Financial Statement",
        "Projected Income Statement",
        "Schedule Of Business Debt Details Of All Business Debts",
        "Articles Of Incorporation",
        "Partnership Agreement"
        ]
    };
    renderTableOnload(data);
});