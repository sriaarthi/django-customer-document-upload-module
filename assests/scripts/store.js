function createInputs () {
    // {"Limited Liability Corp/Partnership": "", "Sole Proprietorship": "", "Phone": "(91) 7348943934", "Partnership": "", "Address": "Endowments Colony, Kakinada - 533003.", "Others": "", "Accountants Name": "Raj", "Email": "p.varun102@gmail.com", "Attorneys Name": "Tarun", "City": "Kakinada", "State": "Andhra Pradesh", "Number of Employees": "3500", "Corporation": "", "Business Type": "Permanent", "Zip": "560103", "Fax": "(6) 323231234", "Bank Name": "Axis Bank", "Company Name": "Varun Puram", "Insurance Agent": "Brahmachaari", "Date of Establishment": "13/12/2019", "References": "", "Loan Requested": "$ 100000000", "Title": "", "Signature": "", "Date": "", "Future Plans": "", "Customer profile": "", "How will the loan requested help the company?": "", "Nature of business and services provided": "", "Will this funding generate employment?": "", "Home Address": "", "Name": "", "SSN": ""};
    var data =  (new Function("return" + document.getElementById('data').value + ";")());

    for (var key of Object.keys(data)) {
        // console.log(key + " -> " + data[key]);
        var container = document.getElementById("confirm-wrapper");

        var content = document.createElement("div");
        content.className = "mat-div";

        var name = document.createElement("div");
        name.innerHTML = key+':';
        name.className = "col s6";
        content.appendChild(name);

        var input = document.createElement("input");
        input.value = data[key];
        input.type = "text";
        input.className = "mat-input col s6";

        content.appendChild(input);
        container.appendChild(content);
    }
};

function createTables() {
    // [{'NAME ': ['Varun  ', 'Harsha  ', 'Gajendra  ', 'Vijayendra  ', 'Ram  ', 'Charan  ', 'Vajra  '], 'TITLE ': ['SE  ', 'SE  ', 'SSE  ', 'TE  ', 'SE  ', 'TSE  ', 'BSE  '], 'PERCENTAGE OWNERSHIP ': ['10%  ', '200  ', '30%  ', '40 %  ', '50%  ', '60%  ', '70%  '], 'ANNUAL COMPENSATION ': ['$ 100  ', '$ 200  ', '$ 300  ', '$ 400  ', '$ 500  ', '$ 600  ', '$ 700  ']}, {'AFFILIATE NAME ': ['Harish  ', 'Girish  ', 'Bharat  ', ' ', ' '], 'RELATED OWNERSHIP ': ['Yes  ', 'No  ', 'Yes  ', ' ', ' '], 'PERCENTAGE OWNERSHIP ': ['25%  ', '10%  ', '25%  ', '%  ', '%  ']}, {'ITEM ': ['Land acquisition  ', 'New construction  ', 'Land and building acquisition  ', 'Building improvements and repairs  ', 'Acquisition of machinery and equipment  ', 'Inventory purchase  ', 'Working capital needs (including accounts payable)  ', 'Acquisition of an existing business  ', 'Repayment or refinancing of debts [+]  ', 'Closing costs  ', 'Other [+]  ', 'TOTAL AMOUNT  '], 'EQUITY ': ['$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  '], 'OTHER SOURCES ': ['$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  '], 'BANK LOAN ': ['$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  '], 'TOTAL COST ': ['$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ', '$ 1000  ']}, {'Descriptio n ': [' ', ' ', ' '], 'Estimated Market Value ': ['$  ', '$  ', '$  '], 'Existing Liens ': ['$  ', '$  ', '$  ']}, {'Name, Address & Social Security Number ': [' ', ' ', ' '], 'Net Worth ': ['$  ', '$  ', '$  ']}, {'': ['Title  ', 'Title  ', 'Title  ', 'Title  ']}, {'Key Customers ': [' ', ' ', ' ', ' ', ' '], 'Key Suppliers ': [' ', ' ', ' ', ' ', ' '], 'Major Competitors ': [' ', ' ', ' ', ' ', ' '], 'Key Risk Factors ': [' ', ' ', ' ', ' ', ' ']}, {'': [' ', ' ', 'SSN:  ', 'Date:  ', 'Name: Signature: Home Address:  ', ' ', ' ', 'SSN:  ', ' ', 'Date:  ']}];
    var tables =  (new Function("return " + document.getElementById('tables').value + ";")());
    
    var html = '';
    var container = document.getElementById("table-wrapper");
    var wrapper = document.createElement("div");
    
    for (var i = 0; i < tables.length; i++)  //Iterating table by table
    {
        // keys are the headings of the tables
        keys = Object.keys(tables[i])
        html += "<table border='1|1' class='highlight'><thead><tr>";

        // Going through the headings of the table
        for (var j = 0; j < keys.length; j++) {
            html += "<th>" + keys[j] + "</th>";
        }
        html += "</tr></thead><tbody>";
        row_length = tables[i][keys[0]].length;
        // Going through the values of the table
        for (var row = 0; row < row_length; row++) {
            html += '<tr>';
            for (var k = 0; k < keys.length; k++) {
                html += "<td>" + tables[i][keys[k]][row] + "</td>";
            }
            html += "</tr>";
        }
        html += "</tbody></table>";
        html += "<br><br>";
    }

    wrapper.innerHTML = html;
    container.appendChild(wrapper);
};

function handleConfirm() {
    alert('Data submitted successfully!!!');
};

$( document ).ready(function() {
    createInputs();
    createTables();
});
