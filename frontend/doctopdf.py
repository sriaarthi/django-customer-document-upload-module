import requests
import argparse
import os
from pdf2image import convert_from_path
import comtypes.client

class FileTypeConversion():

	def doc_to_pdf(self, input_file, output_file):

		url = 'http://34.235.122.44/convert/office'
		files = {'files' : open(input_file,'rb')}
		path = output_file
		# result_file = {'resultFilename' : 'Customer_Data.pdf'}

		r = requests.post(url, files = files)
		print("Got the response")
		data = r.content

		with open(path, 'wb') as wc:
			wc.write(data)

		return output_file

	def pdf_to_img(self, input_file, img_dir_name):

		#Removing the image files
		items = os.listdir(img_dir_name)

		for item in items:
		    if item.endswith(".jpg"):
		        os.remove(os.path.join(img_dir_name,item))
		# #End of Removing

		#Converting pages to .jpg
		pages = convert_from_path(input_file, 500)
		count = 1

		for page in pages:
		    page.save(img_dir_name+'/page_'+str(count)+'.jpg', 'JPEG')
		    count += 1
		# #End of Converting
			


if __name__ == '__main__':

	parser = argparse.ArgumentParser(help="Collecting Arguments")
	parse.add_argument("input_file", type = str, help = "path of the doc file which will be converted to pdf")
	parse.add_argument("output_file", type = str, help = "path of the output pdf file where it will be saved")
	args = parser.parse_args()
	input_file = args.input_file
	output_file = args.output_file

	pdf_file = doc_to_pdf(input_file, output_file)
	pdf_to_img(pdf_file)



	