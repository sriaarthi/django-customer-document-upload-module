from django.shortcuts import render
from django.http import HttpResponse
from .models import Customer
from django.core.files.storage import FileSystemStorage
from .Form_scanner import main
from django.conf import settings
import time
import os
import json


# Create your views here.
def index(request):
    customers = Customer.objects.all()
    return render(request, 'index.html', {'customers': customers})

def upload(request):
    context={}
    if request.method == 'POST':
        uploaded_file = request.FILES['document']
        print(request.FILES.getlist('document'))
        
        fs = FileSystemStorage()
        name = fs.save(uploaded_file.name, uploaded_file)
        time.sleep(5)
        
        context['url'] = fs.url(name)
        file_path = settings.MEDIA_ROOT + "\\" + name
        kvs, tables = main(file_path)
        kvs = json.dumps(kvs)
        filename = os.path.splitext(uploaded_file.name)
        filename = filename[0]
        
        return render(request, 'store.html', {'Data': kvs, 'Table_pages': tables, 'Filename': filename})
    return render(request, 'upload.html')
