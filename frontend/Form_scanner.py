import boto3
import sys
import re
import json
import io, os, sys
from PIL import Image
import base64
from pdf2image import convert_from_path
import glob
import comtypes.client
import argparse
from google.cloud import vision
from google.cloud.vision import types
from .doctopdf import FileTypeConversion
import boto3
from django.conf import settings
import concurrent.futures
import time

class Form_scanner():

    is_form_type = 1
    img_dir_name = settings.MEDIA_ROOT
    key_value_fields = {}
    table_contents = list()

    def remove_old_images(self):

        items = os.listdir(self.img_dir_name)
        
        for item in items:
            if item.endswith(".jpg") or item.endswith(".png"):
                os.remove(os.path.join(self.img_dir_name, item))
        
    def convert_doc_to_img(self, file_name):

        file_type_conv = FileTypeConversion()
        file_name_split = file_name.split('.')
        file_name_split[-1] = 'pdf'
        output_file = '.'.join(file_name_split)
        pdf_file = file_type_conv.doc_to_pdf(file_name, output_file)
        file_type_conv.pdf_to_img(pdf_file, self.img_dir_name)

    def convert_pdf_to_img(self, file_name):

        file_type_conv = FileTypeConversion()
        file_type_conv.pdf_to_img(file_name, self.img_dir_name)


    def get_kv_map(self, imagename):

        key_map = {}
        value_map = {}
        block_map = {}
        table_map = {}
        page_tables = list()

        with open(imagename, 'rb') as file:
            img_read = file.read()
            bytes_read = bytearray(img_read)
            print('Image loaded', imagename)
            
            # process using image bytes
            client = boto3.client('textract')
            response = client.analyze_document(Document={'Bytes': bytes_read}, FeatureTypes=['FORMS','TABLES'])

            # Get the text blocks
            blocks=response['Blocks']


            # get key and value maps
            for block in blocks:
                block_id = block['Id']
                block_map[block_id] = block
                if block['BlockType'] == "PAGE":
                    pass

                if block['BlockType'] == "CELL":
                    pass

                if block['BlockType'] == "LINE":
                    pass

                if block['BlockType'] == "WORD":
                    pass

                if block['BlockType'] == "KEY_VALUE_SET":
                    if 'KEY' in block['EntityTypes']:
                        key_map[block_id] = block
                    else:
                        value_map[block_id] = block

                elif block['BlockType'] == "TABLE":
                    table_map[block_id] = block
            
            if len(key_map) == 0:
                print("---------{}--------\n\nCould not detect any fields in the given form. Please check the file Again\n".format(imagename))
                print("-------------------\n")
                self.is_form_type = 0

        return key_map, value_map, block_map, table_map


    def get_kv_relationship(self, key_map, value_map, block_map, table_map, page_tables):

        kvs = dict()
        tables = ''
        
        for block_id, key_block in key_map.items():
            value_block = self.find_value_block(key_block, value_map)
            key = self.get_text(key_block, block_map)
            val = self.get_text(value_block, block_map)
            kvs[key] = val

        for table_block in table_map.values():
            tables = self.get_text(table_block, block_map, page_tables)

        if len(tables) > 0:
            return tables, kvs

        return None, kvs


    def find_value_block(self, key_block, value_map):
        for relationship in key_block['Relationships']:
            if relationship['Type'] == 'VALUE':
                for value_id in relationship['Ids']:
                    if value_map[value_id]:
                        value_block = value_map[value_id]
                    else:
                        value_block = ''

        return value_block


    def get_text(self, result, blocks_map, page_tables=''):
        text = ''
        table_headings = list()
        table_values = list()
        values = list()
        output_table = dict()
        count = 1
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'CELL':
                            if 'Relationships' in word:
                                for relationship in word['Relationships']:
                                    if relationship['Type'] == 'CHILD':
                                        for child_id in relationship['Ids']:
                                            cell_word = blocks_map[child_id]
                                            if 'Text' in cell_word.keys():
                                                text += cell_word['Text']+ ' '

                            
                            if word['RowIndex'] == 1: # Table Headings
                                
                                table_headings.append(text)
                                text = ''
                            if word['RowIndex'] > 1: # Table Values
                                values.append(text + ' ')
                                text = ''
                                if len(values) == len(table_headings):
                                    table_values.append(values)
                                    values = []
                                    
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                        if word['BlockType'] == 'LINE':
                            text  += word['Text'] + ' '
                        if word['BlockType'] == 'SELECTION_ELEMENT':
                            if word['SelectionStatus'] == 'SELECTED':
                                text += 'X '
                            if word['SelectionStatus'] != 'NOT_SELECTED':
                                text += ''

        #Mapping Column Names and its Values
        if result['BlockType'] == 'TABLE':
            for key in table_headings:
                append_list = list() #List containing the values according to the column index
                for value in table_values:
                    append_list.append(value[table_headings.index(key)])
                    output_table[key] = append_list

            return self.store_all_tables(output_table, page_tables)
        #End of Mapping
                            
        return text

    def store_all_tables(self, output_table, page_tables):

          page_tables.append(output_table)
          return page_tables    


    def print_all_tables(self, page_tables):

        for table in page_tables:
                for key,val in table.items():
                    print(key+" : "+str(val)+"\n")
        with open('content.txt','a+') as add:
            add.write("-------------TABLE VALUES-------------")
            for table in page_tables:
                for key,val in table.items():
                    add.write(str(key+" : "+str(val)+"\n"))
                    

    def print_key_value_fields(self):
        for key, value in self.key_value_fields.items():
                print(key+" : "+value+"\n")
        with open('content.txt','a+') as add:
            add.write("------FIELD VALUES------")
            for key, value in self.key_value_fields.items():
                add.write(str(key+" : "+value+"\n"))

        # decision = 'Y'
        # while decision == "Y" or decision == "y":
        #     search_key = input("Key you want to search: ")
        #     if search_key in key_value_fields.keys():
        #         print("Key found")
        #     else:
        #         print("Key not found")
        #     decision = input("Do you want to search(Y/N): ")

class GoogleVisionOCR():

    def extract_text(self, filename):

        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'Google Vision/image_text_detection_API_token.json'
        client = vision.ImageAnnotatorClient()

        with io.open(filename, 'rb') as image_file:
            content = image_file.read()

        image = vision.types.Image(content=content)
        response = client.document_text_detection(image=image)
        output = response.full_text_annotation.text
        print(output)

textract_obj = Form_scanner()
all_tables_list = list()

def main(filename):

    filename_type = filename.split('.')[-1]
    key_values = ''
    tables = ''
    # import pdb
    # pdb.set_trace()
    
    if filename_type == 'docx':
        print("\n---------File is being converted from doc to image format---------\n")
        textract_obj.remove_old_images()
        textract_obj.convert_doc_to_img(filename)
        images = glob.glob(textract_obj.img_dir_name+"\\*.jpg")
        
    elif filename_type == 'jpg' or filename_type == 'png':
        images = [ filename ]
        # images = glob.glob(textract_obj.img_dir_name+"\\*.jpg")

    elif filename_type == 'pdf':
        print("\n---------File is being converted from pdf to image format---------\n")
        textract_obj.remove_old_images()
        start_time = time.time()
        textract_obj.convert_pdf_to_img(filename)
        images = glob.glob(textract_obj.img_dir_name+"\\*.jpg")

    if os.path.exists('content.txt'):
        os.remove('content.txt')
        print("Deleted old content file")
    with open('content.txt', 'w+') as f:
        print("Create new content file")

    print("Time counter Started")
    start_time = time.time()
    
    # with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
    for table_values, key_value_field in map(execute, images):
        textract_obj.key_value_fields.update(key_value_field)
        textract_obj.table_contents.append(table_values)
        
    # import pdb
    # pdb.set_trace() 

    end_time = time.time()
    print("Time counter Ended")
    print("Program took {} to execute all the images".format(end_time - start_time))

    return textract_obj.key_value_fields, textract_obj.table_contents

def execute(imagename):

    key_map, value_map, block_map, table_map = textract_obj.get_kv_map(imagename)
    page_tables = list()
    # Get Key Value relationship
    if textract_obj.is_form_type:
        return_tables, kvs = textract_obj.get_kv_relationship(key_map, value_map, block_map, table_map, page_tables)

        if return_tables:
            page_tables = return_tables

        if kvs:
            textract_obj.key_value_fields.update(kvs)
        
        print("\n\n== FOUND KEY : VALUE pairs ===\n")

        if page_tables:
            textract_obj.print_all_tables(page_tables)

        textract_obj.print_key_value_fields()

    return page_tables, textract_obj.key_value_fields
