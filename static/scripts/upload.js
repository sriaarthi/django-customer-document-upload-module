function toggleLoader() {
    $('#page-loader').show(); 
    $('#upload-form').hide();
    return true;
};
function clearInput(event) {
    var parentTableRow = $(event.target).parents().eq(1)[0];
    $(parentTableRow.getElementsByClassName('file-path')).val('');
    event.stopPropagation();
};
function renderTable(seletecValue, className) {
    var tableBody = document.getElementsByClassName(className);
    var htmlStr = '';
    htmlStr += '<tr><th>document name</th><th>file to upload</th><th></th></tr>';
    htmlStr += '<tr>';
    htmlStr += '<td class="name-field">' + seletecValue + '</td>';
    htmlStr += '<td class="file-field input-field"><div class="btn btn-small"><span>Browse</span><input type="file" name="document" accept="application/pdf,.doc,.docx"></div><div class="file-path-wrapper"><input class="file-path validate" type="text"></div></td>';
    htmlStr += '<td><i class="material-icons delete" onclick="clearInput(event)">clear</i></td>';
    htmlStr += '</tr>';
    
    // add the htmlstr to tbody
    $(tableBody).html(htmlStr);
};
function handleOnchange(selectElement) {
    var selectedText = selectElement.options[selectElement.selectedIndex].innerHTML;
    var selectedValue = selectElement.value;
    var className = selectElement.id;
    renderTable(selectedValue, className);
};
function renderOptions(data) {
    var optionStr = '';
    var defaultOption = data && data.select ? data.select : "--Documents--";
    var options = data && data.options && data.options.length > 0 ? data.options : [];
    optionStr += '<option value="Documents">' + defaultOption + '</option>';
    // loop through all elements in the array
    for ( i = 0; i < options.length; i++) {
         optionStr += '<option value="'+ options[i] +'">' + options[i] + '</option>';
    };
    return optionStr;
};
$( document ).ready(function() {
    var data = {
        "select": "Mandatory Documents",
        "options": [
            "Loan Application Form",
            "Customer Form"
        ]
    };
    var selectElement = document.getElementById('mandatory-documents');
    // renderOptions
    var optionStr = renderOptions(data);
    // add the options to select
    $(selectElement).html(optionStr);
});
function renderDropdownForSupportingDocuments() {
    var data = {
        "select": "Support Documents", 
        "options": [
          "Form 4506-T : Request For Transcript Of Tax Return",
          "Personal Financial Statement",
          "Projected Income Statement",
          "Schedule Of Business Debt Details Of All Business Debts",
          "Articles Of Incorporation",
          "Partnership Agreement"
        ]
      };
    var parentDiv = document.getElementsByClassName('row-support-document');
    var tableStr = '<table id="support-table" class="support-table"><thead><tr><select id="support-documents" onchange="handleOnchange(this)">';
    // renderOptions
    tableStr += renderOptions(data);
    tableStr += '</select></tr></thead><tbody class="support-documents"></tbody></table>';
    $(parentDiv).append(tableStr);
};