# django-customer-document-upload-module

This module aims at providing the user a document upload UI, where the document that is uploaded is processed and converted to images and the data are read through OCR.

## BASE SETUP:

1. Dowload and install python 3

Download the installer and make sure to click the Add Python to PATH option, which will let you use python directly from the command line.

2. Install Django

$ pip install django

If you get a warning to upgrade pip like the below,
WARNING: You are using pip version 19.2.3, however version 19.3.1 is available.
You should consider upgrading via the 'python -m pip install --upgrade pip' command.

$ python -m pip install --upgrade pip

3. Install boto3, pillow, pdf2image, comtypes for form scanner

$ pip install boto3
$ pip install pillow
$ pip install pdf2image
$ pip install comtypes

4. let’s confirm everything is working by running Django’s local web server.

$ python manage.py runserver
If you visit http://127.0.0.1:8000/ you should see the Django welcome page.

If it throws this error ->  Error: [WinError 10013] An attempt was made to access a socket in a way forbidden by its access permissions, try to run the server in a different port

$ python manage.py runserver 8080
Now if you visit http://127.0.0.1:8080/ you should see the Django welcome page.

## COMMANDS

If not sure about any commands, go for help command
$python manage.py help

To migrate
$python manage.py makemigrations

To collect statics
$python manage.py collectstatic

To runserver
$python manage.py runserver 8080

## FRONTEND 

 1. Javascript & Style files are placed inside static folder static/scripts/xxx and static/css/xxx correspondingly and the html templates are placed inside templates/xxx.
 # Static files (CSS, JavaScript, Images)
 # https://docs.djangoproject.com/en/3.0/howto/static-files/

 - External JavaScript Advantages :
    + Placing scripts in external files has some advantages:

        + It separates HTML and code
        + It makes HTML and JavaScript easier + to read and maintain
        + Cached JavaScript files can speed up page loads
 - To add several script files to one page  - use several script tags:

```        
Example
    <script src="myScript1.js"></script>
    <script src="myScript2.js"></script>
```    

## SOFTWARES

 - Framework: python3.8.0,
 - Editor: VScode
 - Version control: Sourcetree
 
## REFERENCE

 - [Backend Setup](https://djangoforbeginners.com/initial-setup/)
 - [Jinja Template Designer](https://jinja.palletsprojects.com/en/2.10.x/templates/)
 - [Model Fields](https://docs.djangoproject.com/en/3.0/ref/models/fields/)
 - [Poppler](http://blog.alivate.com.au/poppler-windows/)
 - [Sourcetree](https://www.sourcetreeapp.com/)
 